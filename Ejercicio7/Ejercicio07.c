/* 
	Programa: Ejercicio07PerimetroDeUnTriangulo
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que calcule el per�metro de un tri�ngulo e indica que tipo de trinagulo es. 
			El programa pedir� al usuario las entradas necesarias.

*/

//Bibliotecas
	#include <stdio.h>

//Prototipos
int Equilatero(int);
int Isosceles(int, int);
int Escaleno(int, int, int);

//Funcion Principal
int main(){
	
	//variables
	int lado1, lado2, lado3, ladoI, ladoD;
	
	printf("\n\tPrograma que calcula el perimetro de un triangulo e indica que tipo de triangulo es");
	printf("\n\nIngrese el tamano del lado 1 del triangulo: ");
	scanf("%d", &lado1);
	
	printf("\nIngrese el tamano del lado 2 del triangulo: ");
	scanf("%d", &lado2);
	
	printf("\nIngrese el tamano del lado 3 del triangulo: \n");
	scanf("%d", &lado3);
	
	if(lado1 != lado2){
		
		if(lado2 != lado3 ){
		
			if(lado3 != lado1){
			
				printf("El triangulo es ESCALENO y su perimetro es: %d", Escaleno(lado1, lado2, lado3));
			
			}else{
				ladoI = lado3;
				ladoD = lado2;
				printf("\nEl triangulo es ISOSCELES y su perimetro es %d", (Isosceles(ladoI, ladoD)));
			}	
			
		}else{
			ladoI = lado2;
			ladoD = lado1;
			printf("\nEl triangulo es ISOSCELES y su perimetro es %d", (Isosceles(ladoI, ladoD)));
		
		}
		
	}else if (lado2 == lado3){
		
		printf("\nEl triangulo es EQUILATERO y su perimetro  es %d", Equilatero(lado1));
	
	}else{
	
		ladoI = lado1;
		ladoD = lado3;
		printf("\nEl triangulo es ISOSCELES y su perimetro es %d", (Isosceles(ladoI, ladoD)));
	
	}

	return 0;
}

/*
	Funci�n:Equilatero
	Objetivo: calcula el perimetro de un triangulo equilatero
	Par�metros: lado1 indica el tama�o del los lados del triangulo
	Retorna: el perimetro del triangulo

*/
int Equilatero (int lado1){
	return (lado1*3);
}

int Isosceles(int ladoI, int ladoD){
	return ( (ladoI*2) + ladoD);
}

int Escaleno(int lado1, int lado2, int lado3){
	return (lado1 + lado2 + lado3);
}
