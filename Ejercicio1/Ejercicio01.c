/* 
	Programa: Ejercicio01NumerosPares0-100
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que imprima los n�meros pares del 0 al 100.

*/

//Bibliotecas
	#include <stdio.h>

//Funcion Principal
int main(){
	
	//variables
	int i;
	
	printf("Programa que imprime los n�meros pares del 0 al 100.\n");
	
	for(i=0; i<100;i++){
		
		if(i%2){

			printf("%d ", i+1);

		}
		
	}

	return 0;
	
}

