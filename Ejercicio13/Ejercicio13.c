/* 
	Programa: Ejercicio13Ping-Pong
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que imprime los n�meros del 1 al 100, pero aplicando las siguientes reglas.
Regla 1: Cuando el n�mero sea divisible entre 3, en vez del n�mero debe escribir "ping"
Regla 2: Cuando el n�mero sea divisible entre 5, en vez del n�mero debe escribir "pong"
Regla 3: Cuando el n�mero sea divisible entre 3 y tambi�n divisible entre 5, en vez del n�mero debe escribir "ping-pong"

*/

//Bibliotecas
	#include <stdio.h>

//Funcion Principal
int main(){
	
	//variables
	int i;
	
	printf("Programa que imprime los numeros del 0 al 100 ");
	printf("\n Muestra Ping si el numero es divisible entre 3,");
	printf("\n Muestra Pong si el numero es divisible entre 5,");
	printf("\n Y muestra Ping-Pong si el numero es divisible entre ambos (3 y 5)\n");
	
	for(i=1; i<=100;i++){
		
		if(i%3==0 && i%5==0){

			printf("Ping-Pong\n");

		}else if(i%3==0){
			
			printf("Ping\n");
			
		}else if(i%5==0){
			
			printf("Pong\n");
			
		}else{
			
			printf("%d\n", i);
			
		}

	}

	return 0;
	
}

