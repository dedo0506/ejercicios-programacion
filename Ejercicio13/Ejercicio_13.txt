Algoritmo Ejercicio_13Ping-Pong

//variables
	i:entero
	
Inicio
	
	Escibir("Programa que imprime los numeros del 0 al 100 ")
	Escibir("Muestra Ping si el numero es divisible entre 3,")
	Escibir("Muestra Pong si el numero es divisible entre 5,")
	Escibir("Y muestra Ping-Pong si el numero es divisible entre ambos (3 y 5)")
	
	Desde i = 1 hasta i <100 hacer
		
		Sí entonces
		
		Sí No,Sí (i MOD 3 = 0 Y i MOD 5 = 0) entonces
			
			Escibir("Ping-Pong")
		
		Sí No,Sí i MOD 3 = 0 entonces
			
			Escibir("Ping")
		
		Sí No,Sí i MOD 5 = 0 entonces
			
			Escibir("Pong")
		
		De lo contrario entonces
			
			Escibir(i)
			
		Fin Sí
	
	Fin Desde

Fin Algoritmo