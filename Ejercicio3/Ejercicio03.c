/* 
	Programa: Ejercicio03ConversionDolaresAPeso
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que recibe como entrada un monto en d�lares (USD) 
			y devuelva la cantidad equivalente en pesos mexicanos (MXN)

*/

//Bibliotecas
	#include <stdio.h>

//Funcion Principal
int main(){
	
	//variables
	float usd;
	
	printf("\nPrograma que convierte dolares (USD) a pesos (MXN)");
	
	printf("\nIngrese el monto en dolares (USD): ");
	scanf("%f", &usd);
	
	printf("\nEl monto %.2f dolares USD equivale a %.2f pesos MXN ", usd, usd* 20.03);
	
	return 0;
}
