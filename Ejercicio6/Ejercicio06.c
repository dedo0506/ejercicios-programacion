/* 
	Programa: Ejercicio06TrianguloEscaleno
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que calcule el per�metro de un tri�ngulo escaleno. 
			El programa pedir� al usuario las entradas necesarias.

*/

//Bibliotecas
	#include <stdio.h>
	
//Funcion Principal
int main(){
	
	//variables
	int lado1, lado2, lado3;
	
	printf("\n\tPrograma que calcula el perimetro de un triangulo escaleno");
	printf("\n\nIngrese el tamano del lado 1 del triangulo: ");
	scanf("%d", &lado1);
	
	printf("\nIngrese el tamano del lado 2 del triangulo: ");
	scanf("%d", &lado2);
	
	if(lado1 == lado2){
		
		do{
			printf("\nLos lados del triangulo no deben medir lo mismo");
			printf("\nIngrese el tamano del lado 2 del triangulo: ");
			scanf("%d", &lado2);
			
		}while(lado1 == lado2);
		
	}
	
	printf("\nIngrese el tamano del lado 3 del triangulo: ");
	scanf("%d", &lado3);
	
	if(lado3 == lado2 || lado3 == lado1){
		
		do{
			printf("\nLos lados del triangulo no deben medir lo mismo");
			printf("\nIngrese el tamano del lado 3 del triangulo: ");
			scanf("%d", &lado3);
			
		}while(lado3 == lado2 || lado3 == lado1);
		
	}
	
	printf("\nEl perimetro del triangulo es %d", lado1+lado2+lado3);
	
	
	return 0;
}
