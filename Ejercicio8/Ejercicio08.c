/* 
	Programa: Ejercicio08TablasMultiplicar
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que que reciba un n�mero e imprima la tabla de multiplicar de ese n�mero del 2 al 10.

*/

//Bibliotecas
	#include <stdio.h>
//Funcion Principal
int main(){
	
	//variables
	int multiplicando, multiplicador;
	
	printf("\n\tPrograma que apartir de un numero dado muestra su tabla de multiplicar del 2 al 10.\n\n");
	printf("Ingrese el numero :");
	scanf("%d", &multiplicando);
	
	
	printf("\n\tTabla del %d\n\n", multiplicando);
	for(multiplicador= 2; multiplicador <= 10; multiplicador++){
		
		printf("\t%d x %d = %d\n", multiplicando, multiplicador, multiplicando*multiplicador);
	}
		
	return 0;
}

/*	

PROGRAMA QUE MUESTRA TODAS LAS TABLAS DE MULTIPLICAR 

	printf("\n\tTABLAS DE MULTIPLICAR DEL 2 AL 10\n\n");
	
	for(multiplicando = 2; multiplicando <= 10; multiplicando++){
		
		printf("\ttabla del %d\ttabla del %d\ttabla del %d\n", multiplicando, multiplicando+1, multiplicando+2);
		for(multiplicador= 1; multiplicador <= 10; multiplicador++){
			
			printf("\t%d x %d = %d", multiplicando, multiplicador, multiplicando*multiplicador);
			printf("\t%d x %d = %d", multiplicando+1, multiplicador+1, (multiplicando+1)*(multiplicador+1));
			printf("\t%d x %d = %d\n", multiplicando+2, multiplicador+2, (multiplicando+2)*(multiplicador+2));
		}
		printf("\n");
		multiplicando+=2;
	}
*/
	
