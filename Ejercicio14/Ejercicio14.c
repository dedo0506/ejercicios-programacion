/* 
	Programa: Ejercicio14AdivinaNumero
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: en el cual el usuario debe adivinar un n�mero entre 1 y 100. 
			El n�mero es generado aleatoriamente por el programa. 
			El programa solo le dar� 5 intentos al usuario para adivinar

*/

//Bibliotecas
	#include <stdio.h>
	#include <stdlib.h>
	#include <time.h>

//Funcion Principal
int main(){
	
	srand(time(NULL));
	//variables
	int i, num, numero;
	
	printf("Programa en el que debes adivinar un numero\n");
	
	printf("\nEligiendo numero entre el 1 al 100...");
	num = rand() % 101;
	
	printf("\n �Que numero crees que elegi?: ");
	scanf("%d", &numero);
	
	if(numero == num){
	
		printf("Excelente adivinaste a la primera!!");
	
	}else{
		i=5;
		
		while(numero!=num && i>0){
			
			printf("\nVamos sigue intentandolo");
			
			if(numero == num){
				
				printf("Bien adivinaste el numero");
			
			}else if (numero>num){
				
				printf("\nTe pasaste ");
				
			}else if(numero<num){
			
				printf("\nTe falto ");
			
			}
			
			printf("Ese numero no es, tienes %d intentos: ", i);
			printf("\n�Que numero crees que elegi?: ");
			scanf("%d", &numero);
			
			i--;
		}
		if(numero == num){
		
				printf("\nExcelente adivinaste!!\nEl numero era %d",num);
		
		}else{
		
			printf("\nSuerte para la proxima!!\nEl numero era %d",num);
		
		}
	}
	
}

