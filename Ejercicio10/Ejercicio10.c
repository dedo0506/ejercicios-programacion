/* 
	Programa: Ejercicio10A�oBisiesto
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que recibe un n�mero entero que represente un a�o 
				y devuelva verdadero si el a�o es bisiesto.

*/

//Bibliotecas
	#include <stdio.h>

//Funcion Principal
int main(){
	
	//variables
	int anio;
	
	printf("\n\tPrograma que recibe un anio e indica si es bisiesto o no");
	printf("\n\nIngrese el anio: ");
	scanf("%d", &anio);
	
	if(anio%4 == 0){
		
		printf("\nEl anio ingresado es bisiesto");
		
	}else{
		
		printf("\nEl anio ingresado no es bisiesto");
	
	}
	
	return 0;
	
}
