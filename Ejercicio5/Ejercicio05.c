/* 
	Programa: Ejercicio05TrianguloIsosceles
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que calcule el per�metro de un tri�ngulo isosceles. 
			El programa pedir� al usuario las entradas necesarias.

*/

//Bibliotecas
	#include <stdio.h>
	
//Funcion Principal
int main(){
	
	//variables
	int ladoI, ladoD;
	
	printf("\n\tPrograma que calcula el perimetro de un triangulo isosceles");
	printf("\n\nIngrese el tamano de los lados que tienen la misma longitud: ");
	scanf("%d", &ladoI);
	
	printf("\nIngrese el tamano del lado con longitud diferente: ");
		scanf("%d", &ladoD);
		
	if(ladoD == ladoI){
		
		do{
			printf("\nEl lado diferente no puede tener la misma longitud que los lados iguales");
			printf("\nIngrese el tamano del lado diferente del triangulo: ");
			scanf("%d", &ladoD);
			
		}while(ladoD == ladoI);
		
	}
	
	printf("\nEl perimetro del triangulo es %d", ((ladoI*2)+ladoD));
	
	
	return 0;
}
