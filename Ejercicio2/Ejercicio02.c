/* 
	Programa: Ejercicio02SumaDeConsecutivos
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que recibe un n�mero entre 1 y 50 y devuelva 
			la suma de los n�meros consecutivos del 1 hasta ese n�mero.

*/

//Bibliotecas
	#include <stdio.h>

//Funcion Principal
int main(){
	
	//variables
	int numero, aux, i;
	
	printf("\nPrograma que hace la suma de numeros consecutivos del 1 al numero ingresado");
	
	printf("\n Ingrese un numero entre 1 al 50 : ");
	scanf("%d", &numero);
	
		if(numero <=0 || numero >50){
			
			do{
			
				printf("\nEl numero debe de ser entre 1 al 50 ");
		
				printf("\nIngrese nuevamente un numero: ");
				scanf("%d", &numero);
				
			}while(numero <=0 || numero >50);
			
		}
		
		aux=0;
		
		for(i=1; i<numero+1;i++){
			
			printf("\n%d + %d = ", aux, i);
		
			aux+=i;
		
			printf("%d ", aux);
		}
		
		printf("\nEl resultado de la suma consecutiva de 1 a %d es: %d", numero, aux);

		
	return 0;
}

