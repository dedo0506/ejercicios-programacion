/* 
	Programa: Ejercicio12FechasReales
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que recibe un d�a, mes y a�o y devuelva verdadero 
			si la fecha es real y falso si la fecha es irreal. Un ejemplo de 
			fecha irreal es 30 de febrero o 31 de abril o 29 de febrero de 2021.
*/

//Bibliotecas
	#include <stdio.h>
	#include <string.h>

//Funcion Principal
int main(){
	
	//variables
	int dia, mes, anio;
	
		printf("\n\tPrograma que dice si una fecha es real o no\n\n");
		printf("\n\tFormato de fecha DD/MM/AAAA \n(Por favor de separar los datos con /) \nIngrese la fecha: ");
		
		scanf("%d/%d/%d", &dia, &mes, &anio);
			
	
		if(mes==1||mes==3||mes==5||mes==7||mes==8||mes==10||mes==12){
				
			if(dia<=0 || dia >31){
				
				printf("\nLa fecha %d/%d/%d es irreal",dia ,mes ,anio);
				
			}else{
				
				printf("\nLa fecha %d/%d/%d es real",dia ,mes ,anio);
				
			}
	
		}else if(mes==4||mes==5||mes==9||mes==11){
	
			if(dia<=0 || dia >30){
					
				printf("\nLa fecha %d/%d/%d es irreal",dia ,mes ,anio);

			}else{
	
				printf("\nLa fecha %d/%d/%d es real",dia ,mes ,anio);
	
			}				
	
		}else if(mes==2){
		
			if(anio%4 == 0){
				
				if(dia<=0 || dia >29){
				
					printf("\nLa fecha %d/%d/%d es irreal",dia ,mes ,anio);
				
				}else{
					
					printf("\nLa fecha %d/%d/%d es real",dia ,mes ,anio);
				
				}
			
			}else{
			
				if(dia<=0 || dia >28){
					
					printf("\nLa fecha %d/%d/%d es irreal",dia ,mes ,anio);
				
				}else{
					
					printf("\nLa fecha %d/%d/%d es real",dia ,mes ,anio);
				}
				
			}
			
		}else{
			printf("\nLa fecha %d/%d/%d es irreal",dia ,mes ,anio);
		}
					
	return 0;
				
}
			

		

	



