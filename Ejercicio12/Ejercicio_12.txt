Algoritmo Ejercicio_12FechasReales

//variables
	dia, mes, anio: entero
	
Inicio
	
	Escribir("Programa que dice si una fecha es feal o no")
	Escribir("Formato de fecha DD/MM/AAAA(Por favor de separar los datos con /)")
	
	Escribir("Ingrese la fecha: ")
	Leer(dia, mes, anio)
	
	Sí (mes =1 Ó mes =3 Ó mes =5 Ó mes =7 Ó mes =8 Ó mes =10 Ó mes =12) entonces
		
		Sí dia <1 Ó dia > 31 entonces
			
			Escribir("La fecha ", dia, mes, anio, " es irreal")
		
		De lo contrario	
			
			Escribir("La fecha ", dia, mes, anio, " es real")
		
		Fin Sí
		
	Sí No, Sí (mes = 4 Ó mes =6 Ó mes =9 Ó mes =11) entonces
		
		Sí dia <1 Ó dia > 30 entonces
			
			Escribir("La fecha ", dia, mes, anio, " es irreal")
		
		De lo contrario	
			
			Escribir("La fecha ", dia, mes, anio, " es real")
		
		Fin Sí
	
	Sí No, Sí mes = 2 entonces
		
		Sí anio MOD 4 = 0 entonces
			
			Sí dia <1 Ó dia > 29 entonces
			
				Escribir("La fecha ", dia, mes, anio, " es irreal")
		
			De lo contrario	
			
				Escribir("La fecha ", dia, mes, anio, " es real")
		
			Fin Sí
		
		De lo contrario
			
			Sí dia <1 Ó dia > 28 entonces
			
				Escribir("La fecha ", dia, mes, anio, " es irreal")
			
			De lo contrario	
				
				Escribir("La fecha ", dia, mes, anio, " es real")
			
			Fin Sí
		
		Fin Sí
		
	De lo contrario
		
		Escribir("La fecha ", dia, mes, anio, " es irreal")
		
	Fin Sí

Fin Algoritmo