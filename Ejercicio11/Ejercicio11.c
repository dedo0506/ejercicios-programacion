/* 
	Programa: Ejercicio11MesDelA�o
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que recibe un n�mero entero entre 0 y 11, 
			debe devolver el nombre de mes correspondiente. 
			Toma en cuenta que 0 = Enero y 11 = Diciembre.
*/

//Bibliotecas
	#include <stdio.h>
	#include <string.h>

//Funcion Principal
int main(){
	
	//variables
	char* meses[12] ={"enero", "febrero", "marzo", "abril", "mayo", "junio",
		"julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"};
	int mes;
	
	printf("\nPrograma que muestra el nombre del mes del numero ingresado");
	
	printf("\nIngrese el numero del mes: ");
			scanf("%d", &mes);
	
	if(mes <= 0 ||mes > 12){
	
		do{
		
			printf("\nNo existe ese numero de mes");
			printf("\nIngrese nuevamente el numero del mes: ");
			scanf("%d", &mes);		
			
		}while(mes <=0||mes > 12);
		
	}
		
	printf("%d es %s", mes, meses[mes-1]);

}

