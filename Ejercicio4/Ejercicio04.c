/* 
	Programa: Ejercicio04TrianguloEquilatero
	Autor: Davila Olivares Daniela Elizabeth
	Fecha 18/10/2022
	Objetivo: programa que calcule el per�metro de un tri�ngulo equil�tero. 
			El programa pedir� al usuario las entradas necesarias.

*/

//Bibliotecas
	#include <stdio.h>

//Funcion Principal
int main(){
	
	//variables
	int lado;
	
	printf("\n\tPrograma que calcula el perimetro de un triangulo equilatero");
	printf("\n\nIngrese el tama�o de un lado del triangulo: ");
	scanf("%d", &lado);
	
	printf("\nEl perimetro del triangulo equilatero es: %d", lado*3);
	
	
	return 0; 
}
	
