Algoritmo Ejercicio_04TrianguloEquilatero

//variables
	lado:entero

Inicio

	Escribir("Programa que calcula el perimetro de un triangulo equilatero")
	
	Escribir("Ingrese el tamaño de un lado del triangulo: ")
	
	Leer(lado)
	
	Escribir("El perimetro del triangulo equilatero es: ", (lado*3))

Fin Algoritmo